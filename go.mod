module bitbucket.org/uwaploe/dotserver

require (
	github.com/abbot/go-http-auth v0.4.0
	golang.org/x/crypto v0.0.0-20190103213133-ff983b9c42bc // indirect
	golang.org/x/net v0.0.0-20190110200230-915654e7eabc // indirect
)
