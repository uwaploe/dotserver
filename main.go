// Dotserver is an HTTP server that manages file uploads and downloads
// from and to the DOT buoys.
package main

import (
	"context"
	"crypto/rand"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"runtime"
	"syscall"

	auth "github.com/abbot/go-http-auth"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: dotserver [options] rootdir

Stripped down HTTP server to handle file uploads from and configuration
updates to, DOT project buoys.
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	svcAddr = flag.String("addr", ":8081", "HTTP server address, host:port")
	inDir   = flag.String("in", "inbox", "Data inbox directory relative to ROOTDIR")
	outDir  = flag.String("out", "outbox",
		"Configuration outbox directory relative to ROOTDIR")
	pwFile    = flag.String("pw", "htdigest", "Password file path relative to ROOTDIR")
	maxUpload = flag.Int("limit", 2*1024*1024, "Maximum file upload size")
)

// Handle request for system updates for a buoy. The updates file name is the
// buoy account username with ".upd" appended.
func outboxHandler(dir string) auth.AuthenticatedHandlerFunc {
	return auth.AuthenticatedHandlerFunc(func(w http.ResponseWriter, r *auth.AuthenticatedRequest) {
		logRequest(r)
		filePath := filepath.Join(dir, r.Username+".upd")
		file, err := os.Open(filePath)
		if err != nil {
			renderError(w, filePath+" not found", http.StatusNotFound)
			return
		}
		defer file.Close()
		fileStat, err := os.Stat(filePath)
		if err != nil {
			log.Println(err)
		}
		_, filename := path.Split(filePath)
		t := fileStat.ModTime()
		http.ServeContent(w, &(r.Request), filename, t, file)
	})
}

// Handle data file upload from a buoy.
func inboxHandler(dir string, sizeLimit int64) auth.AuthenticatedHandlerFunc {
	return auth.AuthenticatedHandlerFunc(func(w http.ResponseWriter, r *auth.AuthenticatedRequest) {
		logRequest(r)
		// validate file size
		r.Body = http.MaxBytesReader(w, r.Body, sizeLimit)
		if err := r.ParseMultipartForm(sizeLimit); err != nil {
			renderError(w, "Upload too large", http.StatusBadRequest)
			return
		}

		file, _, err := r.FormFile("datafile")
		if err != nil {
			renderError(w, "Invalid file", http.StatusBadRequest)
			return
		}
		defer file.Close()
		fileBytes, err := ioutil.ReadAll(file)
		if err != nil {
			renderError(w, "Invalid file", http.StatusBadRequest)
			return
		}

		fileName := r.PostFormValue("name")
		if fileName == "" {
			filetype := http.DetectContentType(fileBytes)
			var ext string
			switch filetype {
			case "application/x-gzip", "application/gzip":
				ext = ".gz"
			default:
				ext = ".dat"
			}
			fileName = randToken(16) + ext
		}

		dirname := filepath.Join(dir, r.Username)
		os.MkdirAll(dirname, 0755)

		newFile, err := os.Create(filepath.Join(dirname, fileName))
		if err != nil {
			renderError(w, "Cannot write file", http.StatusInternalServerError)
			return
		}
		defer newFile.Close()

		if _, err := newFile.Write(fileBytes); err != nil || newFile.Close() != nil {
			renderError(w, "Cannot write file", http.StatusInternalServerError)
			return
		}
		log.Printf("Uploaded file: %s", fileName)
		w.Write([]byte("success\n"))
	})
}

func logRequest(r *auth.AuthenticatedRequest) {
	log.Printf("%s@%s %s %s", r.Username, r.RemoteAddr, r.Method, r.URL)
}

func renderError(w http.ResponseWriter, message string, statusCode int) {
	log.Printf("ERROR: %d %s", statusCode, message)
	w.WriteHeader(statusCode)
	w.Write([]byte(message))
}

func randToken(len int) string {
	b := make([]byte, len)
	rand.Read(b)
	return fmt.Sprintf("%x", b)
}

func main() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	args := flag.Args()

	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}

	*inDir = filepath.Clean(filepath.Join(args[0], *inDir))
	*outDir = filepath.Clean(filepath.Join(args[0], *outDir))
	*pwFile = filepath.Clean(filepath.Join(args[0], *pwFile))

	authenticator := auth.NewDigestAuthenticator("DOTbuoy",
		auth.HtdigestFileProvider(*pwFile))
	http.HandleFunc("/outbox", authenticator.Wrap(outboxHandler(*outDir)))
	http.HandleFunc("/inbox", authenticator.Wrap(inboxHandler(*inDir, int64(*maxUpload))))

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	svc := &http.Server{
		Addr: *svcAddr,
	}

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			svc.Shutdown(context.Background())
		}
	}()

	log.Fatal(svc.ListenAndServe())
}
