# Server for DOT Buoy Project

This application is a simple HTTP server to manage the data uploads from
and the configuration downloads to the DOT Buoys.

## Usage

``` shellsession
$ dotserver --help
Usage: dotserver [options] rootdir

Stripped down HTTP server to handle file uploads from and configuration
updates to, DOT project buoys.
  -addr string
        HTTP server address, host:port (default ":8081")
  -in string
        Data inbox directory relative to ROOTDIR (default "inbox")
  -limit int
        Maximum file upload size (default 2097152)
  -out string
        Configuration outbox directory relative to ROOTDIR (default "outbox")
  -pw string
        Password file path relative to ROOTDIR (default "htdigest")
  -version
        Show program version information and exit
```

The single required argument is the top-level (root) directory for the
server. The password file must be in the [Apache Htdigest
format](https://en.wikipedia.org/wiki/Digest_access_authentication#The_.htdigest_file).
